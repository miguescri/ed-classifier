import random
import time
from ed_classifier.db import Kfold


def train_and_test(data_train, data_valid, model, train_params):
    random.seed(1)
    trained_model = model.train(data_train, **train_params)
    test_results = model.test(trained_model, data_valid)
    return trained_model, test_results


def kfold(k, data, model, model_params):
    best_params = {}
    best_f1 = 0.0
    fold_len = int(len(data) / k)
    kfolds = []

    for index, params in enumerate(model_params):
        total_f1 = 0.0
        start_time = time.time()
        for i in range(k):
            data_train = data[:i * fold_len] + data[(i + 1) * fold_len:]
            data_valid = data[i * fold_len:(i + 1) * fold_len]
            trained_model, result = train_and_test(data_train, data_valid, model, params)
            total_f1 += result.f1
        end_time = time.time()

        avg_f1 = total_f1 / k
        if avg_f1 > best_f1:
            best_params = params
            best_f1 = avg_f1

        kfolds += [Kfold(id=index, params=params, f1=avg_f1, seconds=(end_time - start_time))]

    return best_params, kfolds


def kfold_and_train(data_train, data_valid, model, params):
    folds = 5

    best_params, kfolds = kfold(folds, data_train, model, params)
    trained_model, result = train_and_test(data_train, data_valid, model, best_params)

    return trained_model, best_params, result, kfolds
