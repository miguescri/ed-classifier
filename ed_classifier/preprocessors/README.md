# Preprocessors

This module contains functions that receive text as input an return that text modified in
some way. All the functions have the following signature:

```python
def function(text: str) -> str:
    # Do something
    return text
```

Available preprocessors:

| Name | Description |
|---|---|
| `none` | Input and output texts are the same |
| `no_newlines` | Newline characters are removed from the input |
| `no_url` | Removes URLs |
| `separate_punctuation` | Add spaces around punctuation symbols to separate them from words |
| `same_case` | Input text is converted to lowercase |
| `no_accents` | Accented characters (*á*,*ö*,*û*...) are transformed to their unaccented version (*a*,*o*,*u*...) |
| `only_letters` | All characters excepting the range [a-zA-Z] are removed |
| `no_anorexia_bulimia` | The words *anorexia* and *bulimia* are removed from the input |
| `no_ana_mia` | The words *ana* and *mia* are removed from the input |
