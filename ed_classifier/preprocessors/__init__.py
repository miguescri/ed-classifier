def apply_preprocessors(preprocessors):
    def apply(text):
        for processor in preprocessors:
            text = processor(text)
        return text

    return apply
