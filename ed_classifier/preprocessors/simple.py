import re
import unidecode


def none(text: str) -> str:
    return text


def no_newlines(text: str) -> str:
    return text.replace('\n', ' ')


def no_url(text: str) -> str:
    # Pattern from https://www.geeksforgeeks.org/python-check-url-string/
    url_pattern = r"(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)" \
                  r"(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+" \
                  r"|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'\".,<>?«»“”‘’]))"
    return re.sub(url_pattern, ' ', text)


def separate_punctuation(text: str) -> str:
    pattern = r"[¡!\"#$%&\'()*+,-./:;<=>¿?@\[\\\]\^_`{|}~]"
    return re.sub(pattern, lambda t: f' {t.group(0)} ', text)


def same_case(text: str) -> str:
    return text.lower()


def no_accents(text: str) -> str:
    return unidecode.unidecode(text)


def only_letters(text: str) -> str:
    return re.sub(r'[^a-zA-Z]', ' ', text)


def no_anorexia_bulimia(text: str) -> str:
    text = text.replace(' anorexia ', '')
    text = text.replace(' bulimia ', '')
    return text


def no_ana_mia(text: str) -> str:
    text = text.replace(' ana ', '')
    text = text.replace(' mia ', '')
    return text
