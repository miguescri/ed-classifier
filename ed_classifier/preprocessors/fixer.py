from collections import Counter
from multiprocessing import Manager
from multiprocessing.context import Process
from typing import List

from numpy import argsort
from textdistance import DamerauLevenshtein


def calculate_wordlist(texts: List[str], min_word_counter: int = 3) -> List[str]:
    counter = Counter()

    for text in texts:
        counter.update(text.split())

    wordlist = [k for k in counter.keys() if counter[k] >= min_word_counter]

    return wordlist


# Set num_process to 0 to avoid multiprocessing
def fix_typos(wordlist: List[str], num_process: int = 0, max_edit_distance: int = 2):
    if num_process < 2:
        num_process = 0

    fixed_words_cache = {}

    def fix(text: str) -> str:
        # The non-local variable keeps the cache between function calls
        nonlocal fixed_words_cache

        # Create data structures depending of whether they have to be concurrent
        if num_process > 0:
            # Managers control the concurrent access to data structures
            manager = Manager()
            cache = manager.dict(fixed_words_cache)
            # process_results contains a list for each process and one extra for the remaining words
            process_results = [manager.list([]) for i in range(num_process + 1)]
        else:
            cache = fixed_words_cache
            process_results = [[]]

        words = text.split()
        words_by_process = int(len(words) / num_process) if num_process > 0 else 0
        words_remaining = len(words) - words_by_process * num_process if num_process > 0 else len(words)

        def f(start_position, items, fixed_words_list, fixed_words_cache):
            dl = DamerauLevenshtein()

            for w in words[start_position: start_position + items]:
                # Check if word is correct
                if w in wordlist:
                    fixed_words_list += [w]

                # Check if word has already been fixed
                elif w in fixed_words_cache:
                    fixed_words_list += [fixed_words_cache[w]]

                # Fix word if it is not correct and has not been fixed
                else:
                    # Calculate the edit distance to every word in the wordlist
                    distances = [dl.distance(w, i) for i in wordlist]
                    # Order the distances from low to high
                    indexes = argsort(distances)

                    # Consider fix only if the edit distance is within limits
                    if distances[indexes[0]] <= max_edit_distance:
                        fixed_word = wordlist[indexes[0]]
                    else:
                        fixed_word = w

                    fixed_words_list += [fixed_word]
                    fixed_words_cache[w] = fixed_word

        # Create a process for each result list, but for the last one, that is for remaining words
        processes = [Process(target=f, args=(i * words_by_process, words_by_process, result, cache)) for i, result in
                     enumerate(process_results[:-1])]

        for t in processes:
            t.start()

        # Fix the words that remain from the split between processes
        if words_remaining > 0:
            f(num_process * words_by_process, words_remaining, process_results[num_process], cache)

        for t in processes:
            t.join()

        fixed_words_cache = cache.copy()
        complete_fixed_words_list = []

        for r in process_results:
            complete_fixed_words_list += r

        return ' '.join(complete_fixed_words_list)

    return fix
