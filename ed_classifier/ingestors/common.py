from typing import Dict, Iterator


def ingest_prompt() -> Iterator[Dict]:
    try:
        while True:
            text = input('--> ')
            yield {'uri': '', 'text': text, 'metadata': None}
    except EOFError:
        pass
