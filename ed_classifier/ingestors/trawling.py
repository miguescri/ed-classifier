from typing import List, Dict, Iterator
import requests
from datetime import datetime


# Generate a timestamp that the API can understand (milliseconds of POSIX epoch)
def datetime_to_ts(dt: datetime) -> int:
    if dt:
        return int(dt.timestamp() * 1000)


def crawl_results(endpoint: str, params: dict) -> Iterator[Dict]:
    # Keep requesting pages while the response contains the filed 'next'
    while endpoint:
        r = requests.get(endpoint, params=params)
        content = r.json()
        next_endpoint = content['response'].get('next')
        if next_endpoint and next_endpoint != '':
            endpoint = next_endpoint
            params = None  # The field 'next' already contains its own params
        else:
            endpoint = None

        data = content['response']['data']
        for entry in data:
            yield entry


def ingest_webpages(token: str, lang: str, keywords: List[str], start_datetime: datetime = None) -> Iterator[Dict]:
    keywords_str = ' OR '.join(keywords)
    query = f'(language:{lang} AND ({keywords_str}))'
    params = {'token': token, 'q': query, 'ts': datetime_to_ts(start_datetime)}

    for entry in crawl_results(f'https://api.trawlingweb.com/', params):
        yield {'uri': entry['url'], 'text': entry['text'], 'metadata': entry}


def ingest_twitter(token: str, worker: str, lang: str, start_datetime: datetime = None) -> Iterator[Dict]:
    for entry in crawl_results(f'https://twitter.trawlingweb.com/posts/{worker}',
                               {'token': token, 'ts': datetime_to_ts(start_datetime)}):
        if entry['lang'] == lang:
            yield {'uri': entry['url'], 'text': entry['text'], 'metadata': entry}


def ingest_instagram(token: str, worker: str, start_datetime: datetime = None) -> Iterator[Dict]:
    for entry in crawl_results(f'https://instagram.trawlingweb.com/posts/{worker}',
                               {'token': token, 'ts': datetime_to_ts(start_datetime)}):
        yield {'uri': entry['url'], 'text': entry['text'], 'metadata': entry}


def ingest_facebook(token: str, worker: str, start_datetime: datetime = None) -> Iterator[Dict]:
    for entry in crawl_results(f'https://facebook.trawlingweb.com/posts/{worker}',
                               {'token': token, 'ts': datetime_to_ts(start_datetime)}):
        yield {'uri': entry['url'], 'text': entry['text'], 'metadata': entry}
