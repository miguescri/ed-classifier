# Ingestors

This module contains generators that stream text documents from a given data source so that 
they can be later classified. All the generators share the following signature:

```python
# Returned Dict must contain at least the key 'text'
def ingest_source(*params) -> Iterator[Dict]
```

Available ingestors:

| Name | Data source | Description | Params |
|---|---|---|---|
| `ingest_webpages` | [TrawlingWeb](https://trawlingweb.com/) | Ingest news sites and blogs | `token: str` valid token for the service, `lang: str` language of the posts to receive, `keywords: List[str]` terms that should appear in the results. |
| `ingest_twitter` | [TrawlingWeb](https://trawlingweb.com/) | Ingest tweets | `token: str` valid token for the service, `worker: str` reference of an active worker, `lang: str` language of the posts to receive |
| `ingest_instagram` | [TrawlingWeb](https://trawlingweb.com/) | Ingest Instagram posts | `token: str` valid token for the service, `worker: str` reference of an active worker |
| `ingest_facebook` | [TrawlingWeb](https://trawlingweb.com/) | Ingest Facebook posts | `token: str` valid token for the service, `worker: str` reference of an active worker |
