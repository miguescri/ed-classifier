import pickle
import math
from . import Dataset, TestResult, Prediction
from typing import IO, NamedTuple
from collections import Counter
from sklearn.metrics import precision_recall_fscore_support

Model = NamedTuple('Model', [('pos_bow', Counter), ('neg_bow', Counter), ('ngram', int)])
name = 'Custom_BoW'


def make_bow_from_text(text: str, ngram: int = 1) -> Counter:
    words_list = text.split()
    if ngram > 1:
        shifted_lists = [words_list[i:] for i in range(ngram)]
        ngram_list = [' '.join(i) for i in zip(*shifted_lists)]
    else:
        ngram_list = words_list
    ngrams_len = len(ngram_list)

    bow = Counter(ngram_list)
    for w in bow:
        # Calculate percentage of each word in the text
        bow[w] /= ngrams_len

    return bow


def bow_dot_product(bow_1: Counter, bow_2: Counter) -> float:
    result = 0.0

    for w in bow_1:
        if w in bow_2:
            result += bow_1[w] * bow_2[w]

    return result


def bow_magnitude(bow: Counter) -> float:
    return math.sqrt(bow_dot_product(bow, bow))


def bow_cosine_similarity(bow_1: Counter, bow_2: Counter) -> float:  # [0,1]
    return bow_dot_product(bow_1, bow_2) / (bow_magnitude(bow_1) * bow_magnitude(bow_2) + 1e-10)


def train(data: Dataset, ngram: int = 1) -> Model:
    pos_text = [i.text for i in data if i.label == '1']
    neg_text = [i.text for i in data if i.label == '0']

    pos_bow = Counter()
    neg_bow = Counter()

    for text_list, bow in [(pos_text, pos_bow), (neg_text, neg_bow)]:
        for text in text_list:
            # Add the wights of each word across all the texts
            bow += make_bow_from_text(text, ngram=ngram)
        for word in bow:
            # Calculate mean of each word's weight across all the texts
            bow[word] /= len(text_list)

    model = Model(pos_bow=pos_bow, neg_bow=neg_bow, ngram=ngram)
    return model


def test(model: Model, data: Dataset) -> TestResult:
    real_labels = [True if i.label == '1' else False for i in data]
    predicted_labels = [predict(model, i.text).promotes_ed for i in data]
    precision, recall, f1, _ = precision_recall_fscore_support(real_labels, predicted_labels, average='binary')

    return TestResult(precision=precision, recall=recall, f1=f1)


def predict(model: Model, text: str) -> Prediction:
    text_bow = make_bow_from_text(text, ngram=model.ngram)
    pos_value = bow_cosine_similarity(text_bow, model.pos_bow)
    neg_value = bow_cosine_similarity(text_bow, model.neg_bow)

    promotes_ed = True if pos_value > neg_value else False
    confidence = max(pos_value, neg_value) / (pos_value + neg_value + 1e-10)

    return Prediction(promotes_ed=promotes_ed, confidence=confidence)


def save(path: str, model: Model) -> None:
    with open(path, mode='wb') as f:
        pickle.dump(model, f)


def load(path: str) -> Model:
    with open(path, mode='rb') as f:
        model = pickle.load(f)
    return model
