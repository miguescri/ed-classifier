# Models

This module contains abstractions to use open source NLP models. All the implementations share 
the following functions signatures and type names:

```python
DataItem = NamedTuple('DataItem', [('text', str), ('label', str)])
Dataset = List[DataItem]
TestResult = NamedTuple('TestResult', [('precision', float), ('recall', float)])
Prediction = NamedTuple('Prediction', [('promotes_ed', bool), ('confidence', float)])

Model = # different in each implementation
name = # name of the implementation

# Train a models using data and the implementation specific params
def train(data: Dataset, *params) -> Model

# Predict the labels of data and calculate their precision
def test(model: Model, data: Dataset) -> TestResult

# Predict the label of text
def predict(model: Model, text: str) -> Prediction

# Save the model to the given disk location
def save(path: str, model: Model) -> None

# Load a model from disk
def load(path: str) -> Model
```

The following models are available:

| Name | Project | Train params |
|---|---|---|
| `custom_bow` | Simple implementation of a bag of words |  |
| `fasttext` | [FastText](https://fasttext.cc/) | `epoch: int` number of iterations (between 5 and 50), `lr: float` learning rate (between 0.1 and 1.0), `ngrams: int` size of the  ngrams (between 1 and 4), `pretrained: str` path to a .vec file containing [pretrained word vectors](https://fasttext.cc/docs/en/crawl-vectors.html) (the dimension of the vectors must be the same as `dim`), `dim: int` the dimension of the word vectors to be trained (between 100 and 300) |
| `spacy` | [SpaCy](https://spacy.io/) | `base_model: str` [pretrained language model](https://spacy.io/models/) to tune (for Spanish `es_core_news_sm`), `cat_type: str` [architecture of the classifier](https://spacy.io/api/textcategorizer/#architectures) (`ensemble`, `simple_cnn` or `bow`), `iterations: int` number of iterations (between 1 and 20) |
| `transformers` | [Transformers](https://huggingface.co/transformers/) |  |
