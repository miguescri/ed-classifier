from typing import List, NamedTuple

DataItem = NamedTuple('DataItem', [('text', str), ('label', str)])
Dataset = List[DataItem]
TestResult = NamedTuple('TestResult', [('precision', float), ('recall', float), ('f1', float)])
Prediction = NamedTuple('Prediction', [('promotes_ed', bool), ('confidence', float)])
