from fasttext import train_supervised, FastText
from sklearn.metrics import precision_recall_fscore_support

from . import Dataset, TestResult, Prediction
from ed_classifier.utils import f1_score
from typing import IO
import tempfile

Model = FastText
name = 'FastText'


def load_fasttext_file(data: Dataset, file: IO) -> IO:
    lines = [f'__label__{item.label} {item.text}\n' for item in data]
    file.writelines(lines)
    return file


def train(data: Dataset,
          epoch: int = 5,
          lr: float = 0.1,
          ngrams: int = 1,
          pretrained: str = None,
          dim: int = None) -> Model:
    tmp_file = tempfile.NamedTemporaryFile()

    with open(tmp_file.name, 'w') as file:
        load_fasttext_file(data, file)

    model = train_supervised(
        tmp_file.name,
        epoch=epoch,
        lr=lr,
        wordNgrams=ngrams,
        pretrainedVectors=pretrained,
        dim=dim
    )
    return model


def test(model: Model, data: Dataset) -> TestResult:
    real_labels = [True if i.label == '1' else False for i in data]
    pred_labels = [predict(model, i.text).promotes_ed for i in data]

    precision, recall, f1, _ = precision_recall_fscore_support(real_labels, pred_labels, average='binary')

    return TestResult(precision=precision, recall=recall, f1=f1)


def predict(model: Model, text: str) -> Prediction:
    prediction = model.predict(text, k=2)
    promotes_ed = True if prediction[0][0] == '__label__1' else False
    confidence = prediction[1][0]
    return Prediction(promotes_ed=promotes_ed, confidence=confidence)


def save(path: str, model: Model) -> None:
    model.save_model(path)


def load(path: str) -> Model:
    return FastText.load_model(path)
