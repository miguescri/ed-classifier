import pickle
import math
from . import Dataset, TestResult, Prediction
from typing import NamedTuple, Dict
from collections import Counter
from sklearn.metrics import precision_recall_fscore_support
from sklearn.feature_extraction.text import TfidfVectorizer

Model = NamedTuple('Model', [('pos', Dict), ('neg', Dict), ('ngram', int)])
name = 'Custom_TF-IDF'


def train(data: Dataset, ngram: int = 1) -> Model:
    pos_text = [i.text for i in data if i.label == '1']
    neg_text = [i.text for i in data if i.label == '0']

    pos_dict = {}
    neg_dict = {}

    # Calculate the TF-IDF matrix with all the texts
    vectorizer = TfidfVectorizer(ngram_range=(ngram, ngram))
    tf_idf_matrix = vectorizer.fit_transform(pos_text + neg_text)
    pos_matrix = tf_idf_matrix[:len(pos_text)]
    neg_matrix = tf_idf_matrix[len(pos_text):]

    # For each class, calculate the average TF-IDF of each word
    for tf_idf, tf_idf_dict in [(pos_matrix, pos_dict), (neg_matrix, neg_dict)]:
        rows, _ = tf_idf.shape
        avg_rows = tf_idf.sum(axis=0) / rows
        tf_idf_dict.update(
            {feature: total for feature, total in zip(vectorizer.get_feature_names(), avg_rows.tolist()[0])})

    model = Model(pos=pos_dict, neg=neg_dict, ngram=ngram)
    return model


def test(model: Model, data: Dataset) -> TestResult:
    real_labels = [True if i.label == '1' else False for i in data]
    predicted_labels = [predict(model, i.text).promotes_ed for i in data]
    precision, recall, f1, _ = precision_recall_fscore_support(real_labels, predicted_labels, average='binary')

    return TestResult(precision=precision, recall=recall, f1=f1)


def predict(model: Model, text: str) -> Prediction:
    words_list = text.split()
    if model.ngram > 1:
        shifted_lists = [words_list[i:] for i in range(model.ngram)]
        ngram_list = [' '.join(i) for i in zip(*shifted_lists)]
    else:
        ngram_list = words_list

    text_counter = Counter(ngram_list)

    pos_value = 0.0
    neg_value = 0.0

    # Add the values of the words present in the text
    for word in text_counter:
        # If the word is in pos, it will also be in neg, because they come from a common matrix
        if word in model.pos:
            pos_value += model.pos[word] * text_counter[word]
            neg_value += model.neg[word] * text_counter[word]

    promotes_ed = True if pos_value > neg_value else False
    confidence = max(pos_value, neg_value) / (pos_value + neg_value + 1e-10)

    return Prediction(promotes_ed=promotes_ed, confidence=confidence)


def save(path: str, model: Model) -> None:
    with open(path, mode='wb') as f:
        pickle.dump(model, f)


def load(path: str) -> Model:
    with open(path, mode='rb') as f:
        model = pickle.load(f)
    return model
