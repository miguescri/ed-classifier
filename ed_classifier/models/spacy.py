import spacy
from spacy.lang.es import Spanish
from spacy.util import minibatch, compounding
from . import Dataset, TestResult, Prediction
import random

Model = Spanish
name = 'SpaCy'


def create_spacy_train_data(data: Dataset):
    train_data = [
        (i.text, {'cats': {'PROMOTES': i.label == '1', 'NOT': i.label == '0'}}) for i in data
    ]
    return train_data


def train(data: Dataset,
          base_model: str = 'es_core_news_sm',
          cat_type: str = 'ensemble',
          iterations: int = 5) -> Model:
    model = spacy.load(base_model)
    if 'textcat' not in model.pipe_names:
        # cat_type defines the kind of architecture of the categorizer:
        # - ensemble : combination of bag of words and neural network
        # - simple_cnn : neural network
        # - bow : bag of words
        textcat = model.create_pipe(
            "textcat", config={'exclusive_classes': True, 'architecture': cat_type, 'positive_label': 'PROMOTES'}
        )
        model.add_pipe(textcat, last=True)
    else:
        textcat = model.get_pipe('textcat')

    textcat.add_label('PROMOTES')
    textcat.add_label('NOT')

    train_data = create_spacy_train_data(data)

    pipe_exceptions = ["textcat", "trf_wordpiecer", "trf_tok2vec"]
    other_pipes = [pipe for pipe in model.pipe_names if pipe not in pipe_exceptions]
    with model.disable_pipes(*other_pipes):  # only train textcat
        optimizer = model.begin_training()
        batch_sizes = compounding(4.0, 32.0, 1.001)
        for i in range(iterations):
            losses = {}
            # batch up the examples using spaCy's minibatch
            random.shuffle(train_data)
            batches = minibatch(train_data, size=batch_sizes)
            for batch in batches:
                texts, annotations = zip(*batch)
                model.update(texts, annotations, sgd=optimizer, drop=0.2, losses=losses)

    return model


def test(model: Model, data: Dataset) -> TestResult:
    result = model.evaluate(create_spacy_train_data(data))

    precision = result.textcat.precision
    recall = result.textcat.recall
    f1 = result.textcat.fscore

    return TestResult(precision=precision, recall=recall, f1=f1)


def predict(model: Model, text: str) -> Prediction:
    result = model(text)
    cats = result.cats
    promotes = cats['PROMOTES'] > cats['NOT']
    confidence = cats['PROMOTES'] if promotes else cats['NOT']
    confidence /= 100

    return Prediction(promotes_ed=promotes, confidence=confidence)


def save(path: str, model: Model) -> None:
    model.to_disk(path)


def load(path: str) -> Model:
    return spacy.load(path)
