from transformers import PreTrainedTokenizer, PreTrainedModel, AutoTokenizer, AutoModelForSequenceClassification, \
    Trainer, TrainingArguments
import torch
from torch.nn.functional import softmax
from sklearn.metrics import accuracy_score, precision_recall_fscore_support
from . import Dataset, TestResult, Prediction
from typing import NamedTuple

Model = NamedTuple('ModelTransformers', [('tokenizer', PreTrainedTokenizer), ('model', PreTrainedModel)])
name = 'Transformers'


class CustomDataset(torch.utils.data.Dataset):
    def __init__(self, dataset: Dataset, tokenizer):
        self.encodings = tokenizer([i.text for i in dataset], truncation=True, padding=True, return_tensors='pt')
        self.labels = [int(i.label) for i in dataset]

    def __getitem__(self, idx):
        item = {key: torch.tensor(val[idx]) for key, val in self.encodings.items()}
        item['labels'] = torch.tensor(self.labels[idx])
        return item

    def __len__(self):
        return len(self.labels)


def compute_metrics(pred):
    labels = pred.label_ids
    preds = pred.predictions.argmax(-1)
    precision, recall, f1, _ = precision_recall_fscore_support(labels, preds, average='binary')
    acc = accuracy_score(labels, preds)
    return {
        'accuracy': acc,
        'f1': f1,
        'precision': precision,
        'recall': recall
    }


def train(data: Dataset,
          pretrained: str = 'bert-base-multilingual-cased',
          num_train_epochs: int = 3,
          per_device_train_batch_size: int = 8,
          per_device_eval_batch_size: int = 8,
          learning_rate: float = 5e-5,
          warmup_steps: int = 0,
          weight_decay: float = 0,
          ) -> Model:
    model = Model(tokenizer=AutoTokenizer.from_pretrained(pretrained),
                  model=AutoModelForSequenceClassification.from_pretrained(pretrained))
    dataset = CustomDataset(data, model.tokenizer)

    training_args = TrainingArguments(
        output_dir='./trained',
        num_train_epochs=num_train_epochs,  # total # of training epochs
        learning_rate=learning_rate,  # Initial learning rate
        per_device_train_batch_size=per_device_train_batch_size,  # batch size per device during training
        per_device_eval_batch_size=per_device_eval_batch_size,  # batch size for evaluation
        warmup_steps=warmup_steps,  # number of warmup steps for learning rate scheduler
        weight_decay=weight_decay,  # strength of weight decay
        logging_dir='./logs',  # directory for storing logs
    )

    trainer = Trainer(
        model=model.model,  # the instantiated 🤗 Transformers model to be trained
        args=training_args,  # training arguments, defined above
        train_dataset=dataset,  # training dataset
    )

    trainer.train()

    return model


def test(model: Model, data: Dataset) -> TestResult:
    dataset = CustomDataset(data, model.tokenizer)

    trainer = Trainer(model=model.model, eval_dataset=dataset, compute_metrics=compute_metrics)
    results = trainer.evaluate()

    precision = results['eval_precision']
    recall = results['eval_recall']
    f1 = results['eval_f1']
    return TestResult(precision=precision, recall=recall, f1=f1)


def predict(model: Model, text: str) -> Prediction:
    tokens = model.tokenizer([text], padding=True, truncation=True, return_tensors='pt')
    result = model.model(**tokens)
    predictions = softmax(result[0], dim=-1)  # Percentage of confidence of each label
    label = int(result[0].argmax(-1))  # Index of the label with highest result

    promotes = False if label == 0 else True
    confidence = float(predictions[0][label])
    return Prediction(promotes_ed=promotes, confidence=confidence)


def save(path: str, model: Model) -> None:
    model.tokenizer.save_pretrained(path)
    model.model.save_pretrained(path)
    return None


def load(path: str) -> Model:
    model = Model(tokenizer=AutoTokenizer.from_pretrained(path),
                  model=AutoModelForSequenceClassification.from_pretrained(path))
    return model
