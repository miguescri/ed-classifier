import csv
import random
from ed_classifier.models import DataItem, Dataset
from typing import Tuple


def load_dataset_from_csv(name: str) -> Dataset:
    with open(name, 'r', newline='') as csvfile:
        reader = csv.reader(csvfile)
        dataset = [DataItem(text=row[0], label=row[1]) for row in reader]
    return dataset


def shuffle_and_split(dataset: Dataset) -> Tuple:
    random.shuffle(dataset)
    data_len = len(dataset)
    data_train = dataset[:int(data_len * 0.8)]
    data_valid = dataset[int(data_len * 0.8):]
    return data_train, data_valid


def f1_score(precision: float, recall: float) -> float:
    return 2 * precision * recall / (precision + recall + 1e-100)
