from ed_classifier.models import Prediction
from typing import NamedTuple, Dict, Iterator
from ed_classifier.preprocessors import apply_preprocessors

Evaluation = NamedTuple(
    'Evaluation',
    [('prediction', Prediction), ('document', Dict)]
)


def classifier(ingestor, preprocessors, model, trained_model) -> Iterator[Evaluation]:
    apply = apply_preprocessors(preprocessors)
    for item in ingestor:
        prep_text = apply(item['text'])
        prediction = model.predict(trained_model, prep_text)

        yield Evaluation(prediction=prediction, document=item)
