from lime.lime_text import LimeTextExplainer
from lime.explanation import Explanation
from typing import Any
import numpy as np


def evaluate(model: Any, trained_model: Any, text: str, save_path: str = None) -> Explanation:
    def predict_proba(text_list):
        result_list = []
        for text in text_list:
            prediction = model.predict(trained_model, text)
            if prediction.promotes_ed:
                result = [1 - prediction.confidence, prediction.confidence]
            else:
                result = [prediction.confidence, 1 - prediction.confidence]
            result_list += [result]

        return np.array(result_list)

    class_names = ['No', 'Promotes']
    explainer = LimeTextExplainer(class_names=class_names)
    exp = explainer.explain_instance(text, predict_proba, num_features=10)

    if save_path:
        exp.save_to_file(save_path)

    return exp
