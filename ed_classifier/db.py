from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine, Column, ForeignKey, Integer, String, Float, DateTime, JSON
from sqlalchemy.orm import relationship, sessionmaker

Base = declarative_base()
Session = sessionmaker()


class Training(Base):
    __tablename__ = 'trainings'

    datetime = Column(DateTime, primary_key=True)
    model = Column(String)
    chain_id = Column(Integer, ForeignKey('chains.id'))
    params = Column(JSON)
    precision = Column(Float)
    recall = Column(Float)
    f1 = Column(Float)
    train_seconds = Column(Float)
    predict_seconds = Column(Float)
    train_items = Column(Integer)
    valid_items = Column(Integer)


class PreChain(Base):
    __tablename__ = 'chains'

    id = Column(Integer, primary_key=True)
    items = Column(Integer)
    seconds_1_round = Column(Float)
    seconds_2_round = Column(Float)


class Preprocessor(Base):
    __tablename__ = 'preprocessors'

    chain_id = Column(Integer, ForeignKey('chains.id'), primary_key=True)
    position = Column(Integer, primary_key=True)
    name = Column(String)

    chain = relationship('PreChain', back_populates='preprocessors')


class Kfold(Base):
    __tablename__ = 'kfolds'

    datetime = Column(DateTime, ForeignKey('trainings.datetime'), primary_key=True)
    id = Column(Integer, primary_key=True)
    params = Column(JSON)
    f1 = Column(Float)
    seconds = Column(Float)

    training = relationship('Training', back_populates='kfolds')


Training.chain = relationship('PreChain')
Training.kfolds = relationship('Kfold', order_by=Kfold.f1, back_populates='training')
PreChain.preprocessors = relationship('Preprocessor', order_by=Preprocessor.position, back_populates='chain')


def create_db(database: str) -> None:
    engine = create_engine(database)
    Base.metadata.create_all(engine)


def bind_engine(database: str) -> None:
    engine = create_engine(database)
    Session.configure(bind=engine)


def get_session() -> Session:
    return Session()


def save_training(database: str, training: Training) -> None:
    bind_engine(database)
    session = get_session()
    session.add(training)
    session.commit()
