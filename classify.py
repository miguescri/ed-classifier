import ed_classifier.preprocessors.simple as pp
from ed_classifier.classify import classifier
from ed_classifier.ingestors.common import ingest_prompt
import ed_classifier.models.fasttext as model

trained_file = 'trained/FastText.bin'

ingestor = ingest_prompt()
preprocessor_list = [
    pp.no_newlines,
    pp.no_url,
    pp.same_case,
    pp.separate_punctuation,
]
trained_model = model.load(trained_file)

print('Write and press enter to classify. Ctrl+D to end.')
for item in classifier(ingestor, preprocessor_list, model, trained_model):
    print(f'Prediction: {item.prediction}')
