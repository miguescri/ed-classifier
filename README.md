# ED Classifier

A NLP classifier that identifies text that promotes eating disorders (anorexia, bulimia...)

## Installation

Required Python >= 3.6

Install the Python dependencies with `pip install -r requirements.txt`

## Train models

If you are going to train your own models, execute `python download.py` inside the `trained/` folder. 
This will download the base pretrained models for Spanish text. Additionally, install the training dependencies with `pip install -r requirements-train.txt`

Run `python train.py` to train the different models. This will train different combinations 
of datasets, text preprocessors and models types, and save the binaries to the `trained/` 
directory. Also, for each combination it is printed in standard output the hyperparameters
selected, the kfold precision and the validation precision.

**THIS MAY TAKE A LONG TIME TO COMPLETE**

## Classify

**All the trained models can be found 
[here](https://drive.google.com/file/d/18w9xqg-r1ycPEeW9b7wimebkDbdXatbp/view?usp=sharing).
The best FastTex model (`FastTex.bin`) can be directly downloaded 
[here](https://drive.google.com/file/d/1k6Yl-9FfWOa_4foSnpXQvn0xt3xh9QTY/view?usp=sharing).**

The file `classify.py` contains an example in which the user is prompted to write texts, which are then classified. Modify it to fit your needs.

The `Dockerfile` provided generates a docker image that runs `classify.py` on start-up. It expects to find a trained FastText model in `trained/FastText.bin`


## Current model results

These are the latest training results obtained:

| Id | Preprocessors | R1 (sec/tw) | R2 (sec/tw) |
| --- | --- | --- | --- |
| 0 | `no_newlines`, `no_url`, `fix` | 0.15055 | 0.00116 |
| 1 | `no_newlines`, `no_url` | 1e-05 | 1e-05 |
| 2 | `no_newlines`, `no_url`, `same_case`, `fix` | 0.12953 | 0.00112 |
| 3 | `no_newlines`, `no_url`, `same_case` | 2e-05 | 2e-05 |
| 4 | `no_newlines`, `no_url`, `same_case`, `separate_punctuation`, `fix` | 0.0774 | 0.0008 |
| 5 | `no_newlines`, `no_url`, `same_case`, `separate_punctuation` | 2e-05 | 2e-05 |
| 6 | `no_newlines`, `no_url`, `same_case`, `no_accents`, `only_letters`, `fix` | 0.06138 | 0.00071 |
| 7 | `no_newlines`, `no_url`, `same_case`, `no_accents`, `only_letters` | 6e-05 | 6e-05 |
| 8 | `no_newlines`, `no_url`, `same_case`, `separate_punctuation`, `no_anorexia_bulimia`, `no_ana_mia`, `fix` | 0.07864 | 0.00082 |
| 9 | `no_newlines`, `no_url`, `same_case`, `separate_punctuation`, `no_anorexia_bulimia`, `no_ana_mia` | 2e-05 | 2e-05 |

| Top | Model | Chain | F1-score | Precision | Recall |
| --- | --- | --- | --- | --- | --- |
| 1 | Transformers | 4 | 0.95975 | 0.96273 | 0.95679 |
| 2 | Transformers | 3 | 0.95766 | 0.95545 | 0.95988 |
| 3 | Transformers | 5 | 0.95766 | 0.95545 | 0.95988 |
| 4 | Transformers | 1 | 0.95483 | 0.96384 | 0.94599 |
| 5 | Transformers | 2 | 0.9526 | 0.94394 | 0.96142 |
| 6 | FastText | 8 | 0.95231 | 0.94939 | 0.95525 |
| 7 | FastText | 4 | 0.95062 | 0.95062 | 0.95062 |
| 8 | FastText | 5 | 0.94842 | 0.94624 | 0.95062 |
| 9 | Transformers | 8 | 0.94557 | 0.95298 | 0.93827 |
| 10 | FastText | 2 | 0.94517 | 0.9459 | 0.94444 |
| 11 | FastText | 9 | 0.9447 | 0.94037 | 0.94907 |
| 12 | FastText | 3 | 0.94316 | 0.93884 | 0.94753 |
| 13 | Transformers | 6 | 0.94238 | 0.96446 | 0.9213 |
| 14 | Transformers | 9 | 0.94136 | 0.95404 | 0.92901 |
| 15 | Transformers | 7 | 0.93913 | 0.96272 | 0.91667 |
| 16 | SpaCy | 5 | 0.93797 | 0.91988 | 0.95679 |
| 17 | SpaCy | 4 | 0.93647 | 0.95215 | 0.9213 |
| 18 | FastText | 0 | 0.93548 | 0.93119 | 0.93981 |
| 19 | Transformers | 0 | 0.93477 | 0.92977 | 0.93981 |
| 20 | FastText | 6 | 0.93085 | 0.9374 | 0.92438 |
| 21 | SpaCy | 6 | 0.92973 | 0.93045 | 0.92901 |
| 22 | SpaCy | 8 | 0.92834 | 0.95888 | 0.89969 |
| 23 | SpaCy | 1 | 0.92625 | 0.95269 | 0.90123 |
| 24 | FastText | 7 | 0.92533 | 0.9232 | 0.92747 |
| 25 | SpaCy | 0 | 0.92482 | 0.97114 | 0.88272 |
| 26 | SpaCy | 2 | 0.92357 | 0.95395 | 0.89506 |
| 27 | SpaCy | 9 | 0.92071 | 0.91859 | 0.92284 |
| 28 | FastText | 1 | 0.91834 | 0.91692 | 0.91975 |
| 29 | SpaCy | 3 | 0.9162 | 0.94876 | 0.8858 |
| 30 | SpaCy | 7 | 0.91099 | 0.94825 | 0.87654 |
| 31 | Custom_BoW | 4 | 0.83082 | 0.90842 | 0.76543 |
| 32 | Custom_BoW | 5 | 0.82185 | 0.90221 | 0.75463 |
| 33 | Custom_BoW | 8 | 0.81857 | 0.90317 | 0.74846 |
| 34 | Custom_TF-IDF | 6 | 0.81825 | 0.782 | 0.85802 |
| 35 | Custom_TF-IDF | 5 | 0.81778 | 0.78632 | 0.85185 |
| 36 | Custom_TF-IDF | 4 | 0.81771 | 0.78359 | 0.85494 |
| 37 | Custom_TF-IDF | 7 | 0.81503 | 0.77997 | 0.8534 |
| 38 | Custom_BoW | 9 | 0.81017 | 0.8985 | 0.73765 |
| 39 | Custom_BoW | 2 | 0.80769 | 0.77557 | 0.84259 |
| 40 | Custom_TF-IDF | 8 | 0.8009 | 0.78038 | 0.82253 |
| 41 | Custom_BoW | 3 | 0.79613 | 0.76868 | 0.82562 |
| 42 | Custom_TF-IDF | 9 | 0.79575 | 0.78326 | 0.80864 |
| 43 | Custom_BoW | 0 | 0.79097 | 0.74897 | 0.83796 |
| 44 | Custom_BoW | 7 | 0.78553 | 0.86322 | 0.72068 |
| 45 | Custom_BoW | 6 | 0.78451 | 0.86296 | 0.71914 |
| 46 | Custom_BoW | 1 | 0.7842 | 0.74548 | 0.82716 |
| 47 | Custom_TF-IDF | 2 | 0.75019 | 0.72993 | 0.7716 |
| 48 | Custom_TF-IDF | 3 | 0.74299 | 0.73025 | 0.75617 |
| 49 | Custom_TF-IDF | 0 | 0.71268 | 0.69343 | 0.73302 |
| 50 | Custom_TF-IDF | 1 | 0.71108 | 0.71384 | 0.70833 |

| Top | Model | Chain | T. chain (sec/tw) | T. model (sec/tw) | Cadence (tw/sec) |
| --- | --- | --- | --- | --- | --- |
| 1 | Transformers | 4 | 0.0008 | 0.10938 | 9 |
| 2 | Transformers | 3 | 2e-05 | 0.11067 | 9 |
| 3 | Transformers | 5 | 2e-05 | 0.11094 | 9 |
| 4 | Transformers | 1 | 1e-05 | 0.11273 | 8 |
| 5 | Transformers | 2 | 0.00112 | 0.10994 | 9 |
| 6 | FastText | 8 | 0.00082 | 2e-05 | 1184 |
| 7 | FastText | 4 | 0.0008 | 4e-05 | 1188 |
| 8 | FastText | 5 | 2e-05 | 4e-05 | 16684 |
| 9 | Transformers | 8 | 0.00082 | 0.11223 | 8 |
| 10 | FastText | 2 | 0.00112 | 2e-05 | 876 |
| 11 | FastText | 9 | 2e-05 | 5e-05 | 13278 |
| 12 | FastText | 3 | 2e-05 | 2e-05 | 28181 |
| 13 | Transformers | 6 | 0.00071 | 0.09746 | 10 |
| 14 | Transformers | 9 | 2e-05 | 0.11223 | 8 |
| 15 | Transformers | 7 | 6e-05 | 0.09882 | 10 |
| 16 | SpaCy | 5 | 2e-05 | 0.01144 | 87 |
| 17 | SpaCy | 4 | 0.0008 | 0.01081 | 86 |
| 18 | FastText | 0 | 0.00116 | 2e-05 | 845 |
| 19 | Transformers | 0 | 0.00116 | 0.11164 | 8 |
| 20 | FastText | 6 | 0.00071 | 2e-05 | 1361 |
| 21 | SpaCy | 6 | 0.00071 | 0.0099 | 94 |
| 22 | SpaCy | 8 | 0.00082 | 0.01077 | 86 |
| 23 | SpaCy | 1 | 1e-05 | 0.01111 | 89 |
| 24 | FastText | 7 | 6e-05 | 4e-05 | 10140 |
| 25 | SpaCy | 0 | 0.00116 | 0.01073 | 84 |
| 26 | SpaCy | 2 | 0.00112 | 0.0109 | 83 |
| 27 | SpaCy | 9 | 2e-05 | 0.01147 | 86 |
| 28 | FastText | 1 | 1e-05 | 2e-05 | 28505 |
| 29 | SpaCy | 3 | 2e-05 | 0.01122 | 88 |
| 30 | SpaCy | 7 | 6e-05 | 0.01057 | 94 |
| 31 | Custom_BoW | 4 | 0.0008 | 0.01564 | 60 |
| 32 | Custom_BoW | 5 | 2e-05 | 0.01658 | 60 |
| 33 | Custom_BoW | 8 | 0.00082 | 0.01563 | 60 |
| 34 | Custom_TF-IDF | 6 | 0.00071 | 2e-05 | 1364 |
| 35 | Custom_TF-IDF | 5 | 2e-05 | 2e-05 | 25428 |
| 36 | Custom_TF-IDF | 4 | 0.0008 | 2e-05 | 1218 |
| 37 | Custom_TF-IDF | 7 | 6e-05 | 2e-05 | 13407 |
| 38 | Custom_BoW | 9 | 2e-05 | 0.01689 | 59 |
| 39 | Custom_BoW | 2 | 0.00112 | 0.0166 | 56 |
| 40 | Custom_TF-IDF | 8 | 0.00082 | 2e-05 | 1195 |
| 41 | Custom_BoW | 3 | 2e-05 | 0.01652 | 60 |
| 42 | Custom_TF-IDF | 9 | 2e-05 | 2e-05 | 24254 |
| 43 | Custom_BoW | 0 | 0.00116 | 0.01707 | 54 |
| 44 | Custom_BoW | 7 | 6e-05 | 0.01432 | 69 |
| 45 | Custom_BoW | 6 | 0.00071 | 0.01384 | 68 |
| 46 | Custom_BoW | 1 | 1e-05 | 0.01773 | 56 |
| 47 | Custom_TF-IDF | 2 | 0.00112 | 2e-05 | 877 |
| 48 | Custom_TF-IDF | 3 | 2e-05 | 2e-05 | 29396 |
| 49 | Custom_TF-IDF | 0 | 0.00116 | 1e-05 | 849 |
| 50 | Custom_TF-IDF | 1 | 1e-05 | 2e-05 | 30390 |

| Top | Model | Chain | Parameters |
| --- | --- | --- | --- |
| 1 | Transformers | 4 | `epoch`: 3 |
| 2 | Transformers | 3 | `epoch`: 3 |
| 3 | Transformers | 5 | `epoch`: 3 |
| 4 | Transformers | 1 | `epoch`: 3 |
| 5 | Transformers | 2 | `epoch`: 3 |
| 6 | FastText | 8 | `epoch`: 50, `lr`: 0.5, `ngrams`: 2, `pretrained`: trained/cc.es.100.vec, `dim`: 100 |
| 7 | FastText | 4 | `epoch`: 25, `lr`: 1.0, `ngrams`: 2, `pretrained`: trained/cc.es.300.vec, `dim`: 300 |
| 8 | FastText | 5 | `epoch`: 25, `lr`: 1.0, `ngrams`: 2, `pretrained`: trained/cc.es.300.vec, `dim`: 300 |
| 9 | Transformers | 8 | `epoch`: 3 |
| 10 | FastText | 2 | `epoch`: 5, `lr`: 0.5, `ngrams`: 1, `pretrained`: trained/cc.es.300.vec, `dim`: 300 |
| 11 | FastText | 9 | `epoch`: 25, `lr`: 1.0, `ngrams`: 2, `pretrained`: trained/cc.es.300.vec, `dim`: 300 |
| 12 | FastText | 3 | `epoch`: 50, `lr`: 0.5, `ngrams`: 1, `pretrained`: trained/cc.es.300.vec, `dim`: 300 |
| 13 | Transformers | 6 | `epoch`: 3 |
| 14 | Transformers | 9 | `epoch`: 3 |
| 15 | Transformers | 7 | `epoch`: 3 |
| 16 | SpaCy | 5 | `iterations`: 5, `base_model`: es_core_news_md |
| 17 | SpaCy | 4 | `iterations`: 10, `base_model`: es_core_news_md |
| 18 | FastText | 0 | `epoch`: 25, `lr`: 0.5, `ngrams`: 1, `pretrained`: trained/cc.es.300.vec, `dim`: 300 |
| 19 | Transformers | 0 | `epoch`: 3 |
| 20 | FastText | 6 | `epoch`: 5, `lr`: 0.5, `ngrams`: 2, `pretrained`: trained/cc.es.100.vec, `dim`: 100 |
| 21 | SpaCy | 6 | `iterations`: 5, `base_model`: es_core_news_lg |
| 22 | SpaCy | 8 | `iterations`: 10, `base_model`: es_core_news_lg |
| 23 | SpaCy | 1 | `iterations`: 10, `base_model`: es_core_news_md |
| 24 | FastText | 7 | `epoch`: 5, `lr`: 1.0, `ngrams`: 2, `pretrained`: trained/cc.es.300.vec, `dim`: 300 |
| 25 | SpaCy | 0 | `iterations`: 10, `base_model`: es_core_news_lg |
| 26 | SpaCy | 2 | `iterations`: 10, `base_model`: es_core_news_md |
| 27 | SpaCy | 9 | `iterations`: 10, `base_model`: es_core_news_lg |
| 28 | FastText | 1 | `epoch`: 50, `lr`: 1.0, `ngrams`: 1, `pretrained`: trained/cc.es.300.vec, `dim`: 300 |
| 29 | SpaCy | 3 | `iterations`: 10, `base_model`: es_core_news_md |
| 30 | SpaCy | 7 | `iterations`: 5, `base_model`: es_core_news_md |
| 31 | Custom_BoW | 4 | `ngram`: 2 |
| 32 | Custom_BoW | 5 | `ngram`: 2 |
| 33 | Custom_BoW | 8 | `ngram`: 2 |
| 34 | Custom_TF-IDF | 6 | `ngram`: 2 |
| 35 | Custom_TF-IDF | 5 | `ngram`: 1 |
| 36 | Custom_TF-IDF | 4 | `ngram`: 1 |
| 37 | Custom_TF-IDF | 7 | `ngram`: 2 |
| 38 | Custom_BoW | 9 | `ngram`: 2 |
| 39 | Custom_BoW | 2 | `ngram`: 2 |
| 40 | Custom_TF-IDF | 8 | `ngram`: 1 |
| 41 | Custom_BoW | 3 | `ngram`: 2 |
| 42 | Custom_TF-IDF | 9 | `ngram`: 1 |
| 43 | Custom_BoW | 0 | `ngram`: 2 |
| 44 | Custom_BoW | 7 | `ngram`: 2 |
| 45 | Custom_BoW | 6 | `ngram`: 2 |
| 46 | Custom_BoW | 1 | `ngram`: 2 |
| 47 | Custom_TF-IDF | 2 | `ngram`: 2 |
| 48 | Custom_TF-IDF | 3 | `ngram`: 2 |
| 49 | Custom_TF-IDF | 0 | `ngram`: 1 |
| 50 | Custom_TF-IDF | 1 | `ngram`: 2 |
