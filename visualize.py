from ed_classifier.db import bind_engine, get_session, Training, PreChain
import matplotlib.colors as colors
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

database_location = 'sqlite:///trained/results.sqlite'

bind_engine(database_location)
session = get_session()

if __name__ == '__main__':
    round_decimals = 5

    chains = session.query(PreChain).all()
    trainings = session.query(Training).order_by(Training.f1.desc()).all()

    print('\nChains:\n')
    print('| Id | Preprocessors | 1st round (sec/tweet) | 2nd round (sec/tweet) |')
    print('| --- | --- | --- | --- |')
    for c in chains:
        preps_text = ', '.join([f'`{i.name}`' for i in c.preprocessors])
        sec_1 = c.seconds_1_round / c.items
        sec_2 = c.seconds_2_round / c.items
        print(f'| {c.id} | {preps_text} | {round(sec_1, round_decimals)} | {round(sec_2, round_decimals)} |')

    print('\nResults:\n')
    print('| Top | Model | Preprocessors | F1-score | Precision | Recall |')
    print('| --- | --- | --- | --- | --- | --- |')
    for index, t in enumerate(trainings):
        print(f'| {index + 1} | {t.model} | {t.chain.id} | {round(t.f1, round_decimals)} | '
              f'{round(t.precision, round_decimals)} | {round(t.recall, round_decimals)} |')

    print('\nTimes:\n')
    print('| Top | Model | Preprocessors | Prep time (sec/tweet) | Model time (sec/tweet) | Total (tweet/sec) |')
    print('| --- | --- | --- | --- | --- | --- |')
    for index, t in enumerate(trainings):
        c = t.chain
        prep_time = c.seconds_2_round / c.items
        model_time = t.predict_seconds / t.valid_items
        cadence = 1 / (prep_time + model_time)
        print(f'| {index + 1} | {t.model} | {t.chain.id} | {round(prep_time, round_decimals)} | '
              f'{round(model_time, round_decimals)} | {int(cadence)} |')

    print('\nParameters:\n')
    print('| Top | Model | Preprocessors | Parameters |')
    print('| --- | --- | --- | --- |')
    for index, t in enumerate(trainings):
        preps_text = ', '.join([f'`{i.name}`' for i in t.chain.preprocessors])
        params_text = ', '.join([f'`{i}`: {t.params[i]}' for i in t.params])
        print(f'| {index + 1} | {t.model} | {t.chain.id} | {params_text} |')

    # Plot scatter points
    colors = {
        'Custom_BoW': colors.TABLEAU_COLORS['tab:blue'],
        'Custom_TF-IDF': colors.TABLEAU_COLORS['tab:red'],
        'FastText': colors.TABLEAU_COLORS['tab:green'],
        'SpaCy': colors.TABLEAU_COLORS['tab:orange'],
        'Transformers': colors.TABLEAU_COLORS['tab:brown'],
    }
    markers = {
        0: 'o',
        1: 'v',
        2: '^',
        3: '<',
        4: '>',
    }
    fig, ax = plt.subplots()
    for t in trainings:
        x = t.f1
        y = 1 / (t.chain.seconds_2_round / t.chain.items + t.predict_seconds / t.valid_items)
        label = t.model
        color = colors[t.model]
        marker = markers[t.chain.id // 2]
        line = 'black'
        linewidth = 0.5 if t.chain.id % 2 else 2.5
        scale = 100.0
        ax.scatter(x, y, label=label, color=color, edgecolors=line, s=scale, linewidths=linewidth, marker=marker)

    legend_items = []
    for c in colors:
        legend_items.append(mpatches.Patch(color=colors[c], label=c))
    for m in markers:
        legend_items.append(
            plt.plot([], marker=markers[m], linestyle='', color='black', label=f'C{m * 2} - C{m * 2 + 1}')[0])

    ax.grid(True)
    ax.set_yscale('log')
    ax.set_xlabel('F1-score')
    ax.set_ylabel('Tweets / second')

    plt.legend(handles=legend_items)

    plt.show()
