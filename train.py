import os
import random
from datetime import datetime
import time
from ed_classifier.models import DataItem
import ed_classifier.models.custom_bow as cb
import ed_classifier.models.custom_tfidf as tfidf
import ed_classifier.models.fasttext as ft
import ed_classifier.models.spacy as sp
import ed_classifier.models.transformers as tr
from ed_classifier.preprocessors import apply_preprocessors
from ed_classifier.preprocessors.simple import no_newlines, no_url, separate_punctuation, same_case, no_accents, \
    only_letters, no_anorexia_bulimia, no_ana_mia
from ed_classifier.preprocessors.fixer import calculate_wordlist, fix_typos
from ed_classifier.train import kfold_and_train
from ed_classifier.utils import load_dataset_from_csv, shuffle_and_split
from ed_classifier.db import create_db, save_training, Training, Preprocessor, PreChain
from ed_classifier.evaluators import lime

random.seed(1)
lime_items_amount = 10

cb_params = [{'ngram': n} for n in [1, 2, 3, 4]]
tfidf_params = [{'ngram': n} for n in [1, 2, 3, 4]]
ft_params = [{'epoch': e, 'lr': l, 'ngrams': n, 'pretrained': p, 'dim': d}
             for e in [5, 25, 50]
             for l in [0.1, 0.5, 1.0]
             for n in [1, 2]
             for d, p in [(100, 'trained/cc.es.100.vec'), (300, 'trained/cc.es.300.vec')]
             ]
sp_params = [{'iterations': i, 'base_model': b}
             for i in [1, 5, 10]
             for b in ['es_core_news_sm', 'es_core_news_md', 'es_core_news_lg']
             ]
tr_params = [
    {'num_train_epochs': e, 'per_device_train_batch_size': b, 'per_device_eval_batch_size': b, 'learning_rate': l,
     'weight_decay': w}
    for e in [1, 3, 5]
    for b in [8, 16, 64]
    for l in [5e-5, 0.01, 0.1]
    for w in [0, 0.01]
]

models = [
    (cb, cb_params),
    (tfidf, tfidf_params),
    (ft, ft_params),
    (sp, sp_params),
    (tr, tr_params)
]

preprocessors = [
    [no_newlines, no_url],
    [no_newlines, no_url, same_case],
    [no_newlines, no_url, same_case, separate_punctuation],
    [no_newlines, no_url, same_case, no_accents, only_letters],  # only_letters makes separate_punctuation redundant
    [no_newlines, no_url, same_case, separate_punctuation, no_anorexia_bulimia, no_ana_mia]
]

print('==================================')
print('== ED_CLASSIFIER TRAINING SUITE ==')
print('==================================')
print('\nLoading datasets')
dataset_sad = load_dataset_from_csv('dataset/sad.csv')
dataset_blogs_tweetfied = load_dataset_from_csv('dataset/blogs_tweetfied.csv')
dataset_mix = dataset_sad + dataset_blogs_tweetfied
full_text = [i.text for i in dataset_mix]

data_train, data_valid = shuffle_and_split(dataset_mix)
len_train = len(data_train)
len_valid = len(data_valid)

database_location = 'sqlite:///trained/results.sqlite'
print(f'Creating database in {database_location}')
create_db(database_location)

chain_id = 0
print('Starting trainings')
for preps in preprocessors:
    for fix in [True, False]:
        prep = preps.copy()  # prep is modified inside next if, so we need to keep the original preps separated
        if fix:
            apply = apply_preprocessors(prep)
            wordlist = calculate_wordlist([apply(i) for i in full_text])
            prep += [fix_typos(wordlist)]

        prep_names = ','.join([i.__name__ for i in prep])
        print('\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
        print(f'Preprocessors: {prep_names}')
        print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')

        chain = PreChain(id=chain_id, items=len_train + len_valid)
        chain_id += 1
        chain.preprocessors = [Preprocessor(position=i, name=p.__name__) for i, p in enumerate(prep)]

        apply = apply_preprocessors(prep)

        # Measure average time to preprocess text
        start_time = time.time()
        prep_data_train = [DataItem(text=apply(i.text), label=i.label) for i in data_train]
        prep_data_valid = [DataItem(text=apply(i.text), label=i.label) for i in data_valid]
        end_time = time.time()
        chain.seconds_1_round = end_time - start_time

        # The fix preprocessor caches corrections, so second round will be faster
        if fix:
            start_time = time.time()
            _ = [DataItem(text=apply(i.text), label=i.label) for i in data_train]
            _ = [DataItem(text=apply(i.text), label=i.label) for i in data_valid]
            end_time = time.time()

        chain.seconds_2_round = end_time - start_time

        for model, params in models:
            print(f'\n- Model: {model.name}')

            # Train and measure training duration
            start_time = time.time()
            trained_model, best_params, result, kfolds = kfold_and_train(prep_data_train,
                                                                         prep_data_valid,
                                                                         model, params)
            end_time = time.time()

            # Save metrics
            train_datetime = datetime.now()
            training = Training(
                datetime=train_datetime,
                model=model.name,
                chain=chain,
                params=best_params,
                precision=result.precision,
                recall=result.recall,
                f1=result.f1,
                train_seconds=(end_time - start_time),
                train_items=len_train,
                valid_items=len_valid)

            training.kfolds = kfolds

            # Measure average time to predict
            start_time = time.time()
            _ = [model.predict(trained_model, i.text) for i in prep_data_valid]
            end_time = time.time()

            training.predict_seconds = end_time - start_time

            # Save results to database and binary
            save_training(database_location, training)
            save_location = f'trained/{model.name}_{train_datetime}.bin'
            model.save(save_location, trained_model)
            print(f'  -> Saved in {save_location}')

            # Generate LIME evaluations
            print('  Generating LIME evaluations')

            directory = f'trained/{model.name}_{train_datetime}_LIME'
            os.makedirs(os.path.dirname(f'{directory}/example.html'), exist_ok=True)
            for index, item in enumerate(prep_data_valid[:lime_items_amount]):
                save_location = f'{directory}/{index}.html'
                lime.evaluate(model, trained_model, item.text, save_path=save_location)
                print(f'  -> Saved in {save_location}')
