import csv
from sklearn.metrics import confusion_matrix

round_decimals = 1

datasets = [
    ('SAD', 'sad.csv', 'sad_manual.csv'),
    ('Blogs', 'blogs.csv', 'blogs_manual.csv'),
    ('Blogs-tweet', 'blogs_tweetfied.csv', 'blogs_tweetfied_manual.csv'),
]

correct_tags_percentage = []

for data in datasets:
    with open(data[2], 'r', newline='') as f:
        reader = csv.reader(f)
        rows = [i for i in reader]

    rows_len = len(rows)
    automatic_labels = [int(i[1]) for i in rows]
    manual_labels = [int(i[2]) for i in rows]
    conf_matrix = confusion_matrix(manual_labels, automatic_labels)
    tn = conf_matrix[0][0]
    fn = conf_matrix[1][0]
    fp = conf_matrix[0][1]
    tp = conf_matrix[1][1]

    correct_tags_percentage.append([data[0], rows_len, tp, fp, fn, tn])

print('\nCorrectness percentages\n')
print('| Dataset | Manually tagged rows | TP | FP | FN | TN | Correct percentage |')
print('| --- | --- | --- | --- | --- | --- | --- |')
for info in correct_tags_percentage:
    name = info[0]
    tagged = info[1]
    tp = info[2]
    fp = info[3]
    fn = info[4]
    tn = info[5]
    percentage = (tp + tn) * 100 / tagged
    print(f'| {name} | {tagged} | {tp} | {fp} | {fn} | {tn} | {round(percentage, round_decimals)} % |')

metrics = []

for data in datasets:
    with open(data[1], 'r', newline='') as f:
        reader = csv.reader(f)
        rows = [i for i in reader]
        rows_len = len(rows)
        rows_positive = len([i for i in rows if i[1] == '1'])
        metrics.append([data[0], rows_len, rows_positive])

metrics.append(['Mix', metrics[0][1] + metrics[2][1], metrics[0][2] + metrics[2][2]])

print('\nSize metrics\n')

print('| Dataset | Items | Positive | Negative |')
print('| --- | --- | --- | --- |')
for metric in metrics:
    name = metric[0]
    items = metric[1]
    positive = metric[2]
    positive_percentage = positive * 100 / items
    negative = items - positive
    negative_percentage = negative * 100 / items
    print(
        f'| {name} | {items} | {positive} ({round(positive_percentage, round_decimals)} %) | '
        f'{negative} ({round(negative_percentage, round_decimals)} %) |')
