import csv
import os
import feedparser
import unicodedata
from bs4 import BeautifulSoup

with open('blogs.csv', 'w', newline='') as csvfile:
    writer = csv.writer(csvfile)

    files = [f for f in os.listdir('./blogs/') if f.split('.')[1] == 'rss']

    for f in files:
        print(f)
        d = feedparser.parse('./blogs/' + f)
        label = 1 if f[0] == 'p' else 0
        for i in d['entries']:
            k = i.keys()
            if 'content' in k:
                h = i['content'][0]['value']
            elif 'summary' in k:
                h = i['summary']
            else:
                print('Unknonwn xml structure')
                break
            s = BeautifulSoup(h, features='lxml')
            # It is important to normalize the received text, since there are
            # cases in which a different UTF-8 subset is used instead of italics
            post = unicodedata.normalize('NFKD', s.text)
            post = post.strip()
            if len(post) > 0:
                writer.writerow([post, label])

