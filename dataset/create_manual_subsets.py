import random
import csv

files = [
    ('sad.csv', 'sad_manual.csv'),
    ('blogs.csv', 'blogs_manual.csv'),
    ('blogs_tweetfied.csv', 'blogs_tweetfied_manual.csv'),
]
rows_number = 200

for entry_file, output_file in files:
    with open(entry_file, 'r', newline='') as in_f:
        reader = csv.reader(in_f)
        with open(output_file, 'w', newline='') as out_f:
            writer = csv.writer(out_f)
            rows = [i for i in reader]
            random.seed(1)
            random.shuffle(rows)
            rows = rows[:rows_number]
            for row in rows:
                writer.writerow(row)
