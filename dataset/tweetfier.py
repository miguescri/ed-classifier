import csv

'''
Takes long entries from blogs.csv and splits them in tweet-sized chunks.
'''

with open('blogs.csv', 'r', newline='') as csvfile_blogs:
    reader = csv.reader(csvfile_blogs)
    with open('blogs_tweetfied.csv', 'w', newline='') as csvfile_tweets:
        writer = csv.writer(csvfile_tweets)
        for row in reader:
            text = row[0]
            label = row[1]
            sentences = text.split('.')
            tweet = ''

            out_rows = []

            for sentence in sentences:
                tweet += sentence.strip() + '. '
                if len(tweet) > 280:
                    out_rows.append([tweet, label])
                    tweet = ''

            # If the last chunk of text is too short (less than 5 words/tokens), merge it into the penultimate
            if len(out_rows) > 1 and len(out_rows[-1][0].split()) < 5:
                out_rows[-2][0] = out_rows[-2][0] + out_rows[-1][0]
                out_rows.pop()

            writer.writerows(out_rows)
