# Blogs dataset

These are the RSS feed of a group of blogs found online. The file `sites.md` contains the URLs of the blogs. The RSS file names start with `n_` if they do NOT promote anorexia or bulimia, and they start with `p_` if they do PROMOTE anorexia or bulimia.
