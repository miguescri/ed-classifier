# Build Python wheels
FROM python:3.9 AS build

RUN mkdir -p /app
WORKDIR /app

COPY requirements.txt /app/
RUN python -m venv venv; . venv/bin/activate; pip install --no-cache-dir -r requirements.txt

########################################################################################################################
# Create slim image
FROM python:3.9-slim

RUN mkdir -p /app
WORKDIR /app
# Make the Python venv visible (activate it manually)
ENV PATH="/app/venv/bin:$PATH"
CMD ["python", "classify.py"]

# Copy the trained binary and the compiled wheels
COPY trained/FastText.bin /app/trained/FastText.bin
COPY --from=build /app/venv /app/venv
COPY ed_classifier /app/ed_classifier
COPY classify.py /app
