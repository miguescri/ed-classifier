import spacy.cli
import fasttext
import fasttext.util
import errno


def bin_to_vec(model, file):
    words = model.get_words()
    file.write(str(len(words)) + " " + str(model.get_dimension()))
    for w in words:
        v = model.get_word_vector(w)
        vstr = ""
        for vi in v:
            vstr += " " + str(vi)
        try:
            file.write(w + vstr)
        except IOError as e:
            if e.errno == errno.EPIPE:
                pass


# Download SpaCy model
spacy.cli.download('es_core_news_sm')
spacy.cli.download('es_core_news_md')
spacy.cli.download('es_core_news_lg')

# Download FastText models, reduce dimension and transform to word vectors
fasttext.util.download_model('es', if_exists='ignore')
ft = fasttext.load_model('cc.es.300.bin')
with open('cc.es.300.vec', 'w+') as f:
    bin_to_vec(ft, f)
fasttext.util.reduce_model(ft, 100)
ft.save_model('cc.es.100.bin')
with open('cc.es.100.vec', 'w+') as f:
    bin_to_vec(ft, f)
